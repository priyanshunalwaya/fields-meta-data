import { LightningElement, track } from 'lwc';
import getObjectFields from '@salesforce/apex/displayFieldsMetadataController.getFields';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class DisplayFieldsMetadata extends LightningElement {

    @track objectName;
    @track objFields = [];
    @track showFields= false;
    @track showSpinner = false;

    handleKeyUp(event) {
        const isEnterKey = event.keyCode === 13;
        if(event.target.value=='' || event.target.value==undefined) {
            this.objFields = [] ;
        }
        if (isEnterKey) {
            console.log( event.target.value);
            this.objectName = event.target.value;
            this.getObjectFields();
        }
    }
    getObjectFields(){
        this.showSpinner = true;
        getObjectFields({
            objectLabel : this.objectName
        }).then(result=> {
            this.objFields = JSON.parse(JSON.stringify(result));
            this.showFields = this.objFields.length > 0 ? true : false
            console.log(this.objFields);
            this.showSpinner = false;
        })
        .catch(error=>{
            console.log(JSON.stringify(error));
            const event = new ShowToastEvent({
                title : 'Incorrect Name',
                variant : 'warning',
                message : 'Enter correct name',
            });
            this.dispatchEvent(event);
            this.objFields = [];
            this.showFields = false;
            this.showSpinner = false;
        })
    }
}